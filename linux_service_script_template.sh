#!/bin/sh
### BEGIN INIT INFO
# Provides:          <SCRIPTNAME>
# Required-Start:    $local_fs $network $named $time $syslog
# Required-Stop:     $local_fs $network $named $time $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Description:       <DESCRIPTION> 
### END INIT INFO

# Check for root
if [ "$(id -u)" != "0" ]; then
	echo "Install requires root"
	exit 1
fi

SCRIPT="<PATH TO SCRIPT>"
RUNAS="<RUNAS USER>"

PIDFILE=/var/run/<SCRIPTNAME>.pid

start() {
  if [ -f $PIDFILE ] && kill -0 $(cat $PIDFILE); then
    echo 'Service already running' >&2
    return 1
  fi
  echo 'Starting service…' >&2
  local CMD="$SCRIPT & echo \$!"
  /bin/su -c "$CMD" $RUNAS > $PIDFILE
  cat $PIDFILE
  echo 'Service started' >&2
}

stop() {
  if [ ! -f "$PIDFILE" ] || ! kill -0 $(cat "$PIDFILE"); then
    echo 'Service not running' >&2
    return 1
  fi
  echo 'Stopping service…' >&2
  kill -15 $(cat "$PIDFILE") && rm -f "$PIDFILE"
  echo 'Service stopped' >&2
}

status() {
  if [ -f $PIDFILE ] && kill -0 $(cat $PIDFILE); then
    echo 'Service is running' >&2
  else
    echo 'Service is not running' >&2
  fi
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  restart)
    stop
    start
    ;;
  status)
    status
    ;;
  *)
    echo "Usage: $0 {start|stop|restart}"
esac
